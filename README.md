# iceDate日期选择js插件

#### 官方
+ iceDate 官方网站 [https://www.iceui.cn/iceDate](https://www.iceui.cn/iceDate)
+ iceDate 官方QQ群 324415936

#### 介绍
iceDate是一款简约风格的js日期插件，纯原生JavaScript开发，整个插件只有一个js文件，无任何依赖，不需要引用jQuery、css……等文件，极致精简，体型娇小，简约的唯美设计，简洁、极速！
可自由选择年、月、日、时、分、秒，满足平常日期选择开发需求。


#### 优点
+ 纯原生开发，无任何依赖，冰清玉洁
+ 响应式布局，适应任何分辨率的设备
+ 整个代码不到30k
+ 简约的唯美设计，简洁、极速

#### 最新更新
# iceDate v1.1.3
+ **2020-08-01**
+ [增加] 时间选择的回调函数timeFunc
+ **2020-06-28**
+ [修复] format参数造成undefined的BUG

#### 使用
```html
<!-- input需要指定一个唯一id -->
<!-- 标准时间 -->
<input type="text" id="time" placeholder="Y-M-D H:I:S" class="iceDate-icon"/>

<!-- 年月日选择 -->
<input type="text" id="ymdTime" placeholder="年月日选择" class="iceDate-icon"/>

<!-- 年份选择 -->
<input type="text" id="yTime" placeholder="年份选择" class="iceDate-icon"/>

<!-- 月份选择 -->
<input type="text" id="mTime" placeholder="年份选择" class="iceDate-icon"/>

<!-- 日期选择 -->
<input type="text" id="dTime" placeholder="日期选择" class="iceDate-icon"/>

<!-- 时间选择 -->
<input type="text" id="tTime" placeholder="时间选择" class="iceDate-icon"/>
```
```javascript
//标准时间
ice.date({id:'time'});

//年月日选择
//format参数：y=年 m=月 d=日 h=时 i=分 s=秒
ice.date({id:'ymdTime',format:'y-m-d'});

//年份选择
ice.date({id:'yTime',view:'year'});

//月份选择
ice.date({id:'mTime',view:'month'});

//日期选择
ice.date({id:'dTime',view:'day'});

//时间选择
ice.date({id:'tTime',view:'time'});
```

#### 参数设置
```javascript
/**
 * 传参为json格式，参数如下
 *  ├ id        {id}       input的id
 *  ├ format    {string}   时间格式 y=年 m=月 d=日 h=时 i=分 s=秒  例如：y-m-d 格式化为 2020-04-13
 *  ├ view      {string}   默认视图
 *  ├ func      {function} 确认的回调函数
 *  ├ yearFunc  {function} 选择年份后的回调函数
 *  ├ monthFunc {function} 选择月份后的回调函数
 *  ├ dayFunc   {function} 选择日期后的的回调函数
 *  └ timeFunc  {function} 选择时间后的的回调函数
 *
 * 回调函数：func yearFunc monthFunc dayFunc 返回的参数如下：
 *	├ elem 当前的input对象
 *	├ time 格式化后的时间
 *	├ date 时间对象
 *	└ timestamp 时间戳
 */

//例如：
ice.date({id:'time',func:function(e){
	alert(e.time);
}});
```